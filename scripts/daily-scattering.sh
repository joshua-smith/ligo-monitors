#!/bin/bash
#
# Run the gwdetchar-scattering code on a daily stride

export MPLCONFIGDIR=${HOME}/.matplotlib
. ~/.bash_profile
if [ "$USER" = "detchar" ]; then
    unset X509_USER_PROXY
fi
set -e

PYTHON_VERSION=$(
    python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
GWPYSOFT_VIRTUAL_ENV="/home/detchar/opt/gwpysoft-${PYTHON_VERSION}"
. ${GWPYSOFT_VIRTUAL_ENV}/bin/activate
echo "-- Environment set"

here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# get run date
gpsstart=`gps_start_yesterday`
gpsend=`gps_start_today`
date_=`yyyymmdd ${gpsstart}`
duration=$((gpsend-gpsstart))
echo "-- Identified run date as ${date_}"

# set up output directory
htmlbase=${HOME}/public_html/scattering/
outdir=${htmlbase}/day/${date_}
mkdir -p ${outdir}
cd ${outdir}
echo "-- Moved to output directory: `pwd`"

# get IFO
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi

# set parameters
NPROC=4
STATEFLAG="${IFO}:DMT-UP:1"
FREQUENCY="15"

# run the code
set +e

nagios_status 0 "Daily scattering analysis for ${date_} is running" 10000 "Daily scattering analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

cmd="gwdetchar-scattering $gpsstart $gpsend --ifo ${IFO} --main-channel ${IFO}:GDS-CALIB_STRAIN --frametype ${IFO}_R --state-flag ${STATEFLAG} --nproc ${NPROC} --frequency-threshold ${FREQUENCY} --verbose"
echo "$ $cmd" && eval $cmd 2> /tmp/daily-scattering-$USER.err

EXITCODE="$?"
deactivate

# write JSON output
TIMER=100000
TIMEOUT="Daily scattering analysis has not run since ${date_}"
if [ "$EXITCODE" -eq 0 ]; then
    MESSAGE="Daily scattering analysis for ${date_} complete"
    nexit=0
else
    MESSAGE=`echo -n "Daily scattering analysis for ${date_} failed with exitcode $EXITCODE\\\n" && sed ':a;N;$!ba;s/\n/\\\n/g' /tmp/daily-scattering-$USER.err | tr '"' "'"`
    nexit=2
fi

nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
exit ${EXITCODE}
