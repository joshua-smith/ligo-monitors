#!/bin/bash
#
# Run the hveto code on a daily stride
#

# -- set up -------------------------------------------------------------------

. ~/.bash_profile
if [ "$USER" = "detchar" ]; then
    unset X509_USER_PROXY
fi
set -e

# set python environment
PYTHON_VERSION=$(
    python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
GWPYSOFT_VIRTUAL_ENV="/home/detchar/opt/gwpysoft-${PYTHON_VERSION}"
. ${GWPYSOFT_VIRTUAL_ENV}/bin/activate
echo "-- Environment set"

# set bash environment
here_=$(cd "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))" && pwd)
. ${here_}/functions
echo "-- Loaded local functions"

# -- set condor accounting information ----------------------------------------
# these data should match the parameters in the /condor/daily-hveto.sub file
#
# these are picked up by wdq-batch for the omega scans

export _CONDOR_ACCOUNTING_TAG="ligo.prod.o2.detchar.dqtriggers.hveto"
export _CONDOR_ACCOUNTING_USER="joshua.smith"

# -- get run times ------------------------------------------------------------
# logic goes like this
#     - if we're less than 8 hours into a day, presume we want to process
#       all of yesterday
#     - if we're 8 < us < 16 hours into a day, process the first 8 hrs of today
#     - if we're 16 < us < 24 hours into a day, process the first 16 hrs

if [ "$#" -ne 0 ]; then  # user specified date
    nagios=0  # don't write nagios JSON files
    date_=$1
    year=${date_:0:4}
    month=${date_:4:2}
    day=${date_:6:2}
    gpsstart=`lalapps_tconvert $year/$month/$day`
    gpsend=`lalapps_tconvert $year/$month/$day + 1day`
    duration=$((gpsend-gpsstart))
else
    nagios=1  # write nagios JSON files
    now=`lalapps_tconvert`
    startofday=`gps_start_today`
    elapsed=$((now-startofday))
    echo "Now: $now"
    echo "Today: $startofday"
    echo "Elapsed: $elapsed"
    if [[ "$elapsed" -lt 28800 ]]; then
        gpsstart=`gps_start_yesterday`
        gpsend=$startofday
        duration=$((gpsend-gpsstart))
    elif [[ "$elapsed" -lt 57600 ]]; then
        gpsstart=$startofday
        duration=28800
        gpsend=$((gpsstart+duration))
    else
        gpsstart=$startofday
        duration=57600
        gpsend=$((gpsstart+duration))
    fi
    date_=`yyyymmdd $gpsstart`
fi

echo "-- Identified run date as ${date_}"
echo "Will process $gpsstart-$gpsend"
echo "Duration: $duration"

# -- create directories -------------------------------------------------------

htmlbase=${HOME}/public_html/hveto/

# create day dir
daydir=${htmlbase}/day/${date_}
mkdir -p $daydir
cd $daydir

# create dir for this run
gpstag="$gpsstart-$gpsend"
mkdir -p $gpstag
outdir=${htmlbase}/day/${date_}/$gpsstart-$gpsend
mkdir -p ${outdir}

# link this run as 'latest'
latest="latest"
[[ -L $latest ]] && unlink $latest
ln -s $gpstag $latest

cd $gpstag

echo "-- Moved to output directory: `pwd`"

# get IFO and FECs
if [[ "`hostname -f`" == *"ligo-la"* ]]; then
    export IFO="L1"
elif [[ "`hostname -f`" == *"ligo-wa"* ]]; then
    export IFO="H1"
else
    echo "Cannot determine IFO" 1>&2 && false
fi
set +e

# -- run ----------------------------------------------------------------------

# set hveto parameters
configuration="${here_}/../configurations/hveto/h1l1-hveto-daily-o2.ini"
ncpu=4

# run the code
[[ $nagios -ne 0 ]] && nagios_status 0 "Daily Hveto analysis for ${date_} is running" 10000 "Daily Hveto analysis for ${date_} is taking too long" > ${htmlbase}/nagios.json

cmd="hveto $gpsstart $gpsend --ifo ${IFO} --config-file $configuration --nproc $ncpu --omega-scans 5"
echo "$ $cmd" && eval $cmd 2> /tmp/daily-hveto-$USER.err

EXITCODE="$?"
echo "-- Hveto has exited with code $EXITCODE"

# submit omega scans
omegadag="./scans/condor/wdq-batch.dag"
if [[ $EXITCODE -eq 0 ]] && [[ -f $omegadag ]]; then
    echo "-- Submitting condor DAG for omega scans..."
    echo "$ condor_submit_dag -force $omegadag"
    condor_submit_dag -force $omegadag
    #[[ $nagios -ne 0 ]] && nagios_status 0 "Daily Hveto Omega scans for ${date_} are running" 20000 "Daily Hveto Omega scans for ${date_} are taking too long" > ${htmlbase}/nagios.json
    #sleep 10
    #while [ -f ${omegadag}.lock ]; do
    #    sleep 30
    #done
    #echo "Condor DAG has exited"
    #if [ -f ${omegadag}.rescue001 ]; then
    #    echo "Something broke, the rescue DAG was generated."
    #    OEXITCODE=1
    #else
    #    OEXITCODE=0
    #fi
    [ "$?" -eq 0 ] && OEXITCODE=0 || OEXITCODE=1
elif [ $EXITCODE -eq 0 ]; then
    OEXITCODE=0
fi

# -- post-process -------------------------------------------------------------

deactivate

# write JSON output
if [[ $nagios -ne 0 ]]; then
    TIMER=40000
    TIMEOUT="Daily Hveto analysis has not run since ${date_}"
    if [ "$EXITCODE" -ne 0 ]; then
        MESSAGE="Daily Hveto analysis for ${date_} failed with exitcode $EXITCODE\\\nSee log file under /tmp/daily-hveto-$USER.err on `hostname -f`"
        nexit=2
    elif [ "$OEXITCODE" -ne 0 ]; then
        dagpath=`cd $(dirname $omegadag) && pwd`
        MESSAGE="Daily Hveto omega scans errored\\\nSee DAG data under $dagpath"
        nexit=1
    else
        MESSAGE="Daily Hveto analysis for ${date_} complete"
        nexit=0
    fi
    nagios_status $nexit "$MESSAGE" $TIMER "$TIMEOUT" > ${htmlbase}/nagios.json
fi

# exit
exit ${EXITCODE}
